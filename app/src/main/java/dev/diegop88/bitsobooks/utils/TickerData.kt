package dev.diegop88.bitsobooks.utils

import dev.diegop88.bitsobooks.domain.entities.Ticker

sealed class TickerData {
    object Loading : TickerData()

    class Success(val ticker: Ticker?) : TickerData()

    class Error(val error: Throwable) : TickerData()
}
