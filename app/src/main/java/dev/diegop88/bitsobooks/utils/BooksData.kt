package dev.diegop88.bitsobooks.utils

import dev.diegop88.bitsobooks.domain.entities.Book

sealed class BooksData {

    object Loading : BooksData()

    class Success(val books: List<Book>) : BooksData()

    class Error(val error: Throwable) : BooksData()
}
