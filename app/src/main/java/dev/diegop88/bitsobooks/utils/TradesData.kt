package dev.diegop88.bitsobooks.utils

import dev.diegop88.bitsobooks.domain.entities.Trade

sealed class TradesData {
    object Loading : TradesData()

    class Success(val tradesList: List<Trade>) : TradesData()

    class Error(val error: Throwable) : TradesData()
}
