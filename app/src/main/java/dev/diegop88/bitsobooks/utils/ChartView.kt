package dev.diegop88.bitsobooks.utils

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import dev.diegop88.bitsobooks.domain.entities.Trade
import kotlin.math.min


class ChartView(ctx: Context, attrs: AttributeSet) : View(ctx, attrs) {

    var listener: OnDataSelectedListener? = null

    class OnDataSelectedListener(val onDataSelected: (String, String) -> Unit)

    inner class Step(
        val date: String,
        val price: String,
        val drawXPoint: Float,
        val drawYPoint: Float = 0f,
        longStep: Float
    ) {
        private val minRange: Float = drawXPoint - (longStep / 2)
        private val maxRange: Float = drawXPoint + (longStep / 2)

        fun contains(actualPoint: Float) = minRange <= actualPoint && actualPoint < maxRange
    }

    private val linePaint = Paint()

    private var drawXMin: Float = 0f
    private var drawXMax: Float = 0f
    private var drawYMin: Float = 0f
    private var drawYMax: Float = 0f

    private var data = mutableListOf<Trade>()
    private val steps = mutableListOf<Step>()

    init {
        linePaint.color = Color.BLUE
        linePaint.isAntiAlias = true
        linePaint.strokeWidth = 5f
    }

    fun setColor(color: String?) {
        linePaint.color = Color.parseColor(color)
    }

    fun setData(bookTrades: List<Trade>) {
        data.clear()
        data.addAll(bookTrades)
        setScale()
        showLast()
    }

    override fun onTouchEvent(event: MotionEvent?) =
        if (listener == null) super.onTouchEvent(event) else when (event?.action) {
            MotionEvent.ACTION_DOWN -> handleData(event)
            MotionEvent.ACTION_MOVE -> handleData(event)
            MotionEvent.ACTION_UP -> showLast()
            else -> super.onTouchEvent(event)
        }

    private fun showLast(): Boolean {
        listener?.let {
            val step = steps.last()
            it.onDataSelected(step.date, step.price)
            return true
        }
        return false
    }

    private fun handleData(event: MotionEvent): Boolean {
        listener?.let {
            for (step in steps) {
                if (step.contains(event.x)) {
                    it.onDataSelected(step.date, step.price)
                }
            }
            return true
        }

        return super.onTouchEvent(event)
    }

    override fun onDraw(canvas: Canvas?) {
        for (i in steps.indices) {
            if (i < steps.size - 1) {
                val first = steps[i]
                val second = steps[i + 1]
                canvas?.drawLine(
                    first.drawXPoint,
                    first.drawYPoint,
                    second.drawXPoint,
                    second.drawYPoint,
                    linePaint
                )
            }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
//        super.onSizeChanged(w, h, oldw, oldh)

        drawXMin = (left + paddingStart).toFloat()
        drawXMax = (right - paddingEnd).toFloat()

        drawYMin = top.toFloat()
        drawYMax = bottom.toFloat()

        setScale()
    }

    override fun onVisibilityChanged(changedView: View, visibility: Int) {

        super.onVisibilityChanged(changedView, visibility)
    }

    private fun setScale() {
        if (data.isEmpty())
            return

        steps.clear()

        val pricesValues = getPricesValues()

        //Range X axis
        val range = pricesValues.size
        val drawRange = drawXMax - drawXMin
        val longStep = drawRange / range

        //Range Y axis
        val minValue = pricesValues.min() ?: 0.0
        val maxValue = pricesValues.max() ?: 1.0
        val yRange = maxValue.minus(minValue)
        val yDrawRange = drawYMax - drawYMin

        var actualDrawStep = drawXMin

        for (i in pricesValues.indices) {
            val value = pricesValues[i] - minValue
            val trade = data[i]
            val percentage = value.div(yRange)
            val adjust = 1 - percentage
            val yPos = (adjust * yDrawRange).toFloat()

            steps.add(Step(trade.date, trade.price, actualDrawStep, yPos, longStep))
            actualDrawStep += longStep
        }

        invalidate()
    }

    private fun getPricesValues() = data.map { it.priceValue }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val height = getDimension(
            heightMeasureSpec,
            TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                100f,
                context.resources.displayMetrics
            )
        )
        val width = getDimension(
            widthMeasureSpec,
            TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                100f,
                context.resources.displayMetrics
            )
        )
        setMeasuredDimension(width, height)
    }

    private fun getDimension(measureSpec: Int, desired: Float): Int {
        val dimension = MeasureSpec.getSize(measureSpec)
        return when (MeasureSpec.getMode(measureSpec)) {
            MeasureSpec.EXACTLY -> dimension
            MeasureSpec.AT_MOST -> min(dimension.toFloat(), desired).toInt()
            else -> desired.toInt()
        }
    }
}
