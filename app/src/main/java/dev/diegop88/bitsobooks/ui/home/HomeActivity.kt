package dev.diegop88.bitsobooks.ui.home

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import dev.diegop88.bitsobooks.R
import dev.diegop88.bitsobooks.domain.entities.Book
import dev.diegop88.bitsobooks.ui.detail.DetailActivity
import dev.diegop88.bitsobooks.utils.BooksData
import kotlinx.android.synthetic.main.activity_home.*
import org.koin.androidx.scope.currentScope

class HomeActivity : AppCompatActivity() {

    private val viewModel: HomeViewModel by currentScope.inject()

    private val booksAdapter = BooksAdapter(::onBookClick)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        viewModel.booksLiveData.observe(this, Observer {
            when (it) {
                is BooksData.Loading -> showLoading()
                is BooksData.Success -> showSuccess(it.books)
                is BooksData.Error -> showError(it.error)
            }
        })

        with(books_list) {
            layoutManager = GridLayoutManager(context, 2)
            adapter = booksAdapter
        }

        books_refresh.setOnRefreshListener {
            viewModel.getBooksAndTickers()
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.getBooksAndTickers()
    }

    private fun showLoading() = books_refresh.post { books_refresh.isRefreshing = true }

    private fun showSuccess(books: List<Book>) =
        booksAdapter.submitList(books).also { books_refresh.isRefreshing = false }

    private fun showError(error: Throwable) =
        Snackbar.make(books_refresh, "Error: ${error.message}", Snackbar.LENGTH_LONG).show().also {
            books_refresh.isRefreshing = false
        }

    private fun onBookClick(book: Book) =
        startActivity(Intent(this, DetailActivity::class.java).apply {
            putExtra("Book", book)
        })
}
