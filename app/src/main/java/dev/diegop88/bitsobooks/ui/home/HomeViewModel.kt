package dev.diegop88.bitsobooks.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dev.diegop88.bitsobooks.domain.usecases.GetBooks
import dev.diegop88.bitsobooks.utils.BooksData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class HomeViewModel(private val getBooks: GetBooks) : ViewModel() {

    private val _booksLiveData = MutableLiveData<BooksData>()
    val booksLiveData: LiveData<BooksData>
        get() = _booksLiveData

    fun getBooksAndTickers() = GlobalScope.launch {
        _booksLiveData.postValue(BooksData.Loading)
        try {
            val result = getBooks()
            _booksLiveData.postValue(BooksData.Success(result))
        } catch (exception: IOException) {
            _booksLiveData.postValue(BooksData.Error(exception))
        }
    }
}
