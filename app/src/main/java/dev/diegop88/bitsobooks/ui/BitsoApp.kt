package dev.diegop88.bitsobooks.ui

import android.app.Application
import dev.diegop88.bitsobooks.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class BitsoApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@BitsoApp)
            modules(appModule)
        }
    }
}