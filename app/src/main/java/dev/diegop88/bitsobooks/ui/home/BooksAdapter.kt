package dev.diegop88.bitsobooks.ui.home

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import dev.diegop88.bitsobooks.R
import dev.diegop88.bitsobooks.domain.entities.Book
import kotlinx.android.synthetic.main.item_book.view.*

class BooksAdapter(private val onBookClick: (Book) -> Unit) :
    ListAdapter<Book, BooksAdapter.BookVieWHolder>(Book.diff) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = with(parent.context) {
        BookVieWHolder(LayoutInflater.from(this).inflate(R.layout.item_book, parent, false))
    }

    override fun onBindViewHolder(holder: BookVieWHolder, position: Int) =
        holder.bind(getItem(position))

    inner class BookVieWHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(book: Book) = with(itemView) {
            setOnClickListener { onBookClick(book) }

            book_title.text = book.book
            book_lastPrice.text = book.lastPrice

            book_title.setTextColor(Color.parseColor(book.color))
            book_lastPrice.setTextColor(Color.parseColor(book.color))
        }
    }
}