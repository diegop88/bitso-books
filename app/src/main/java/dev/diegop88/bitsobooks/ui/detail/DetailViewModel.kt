package dev.diegop88.bitsobooks.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dev.diegop88.bitsobooks.domain.usecases.GetBookTrades
import dev.diegop88.bitsobooks.domain.usecases.GetTickers
import dev.diegop88.bitsobooks.utils.TickerData
import dev.diegop88.bitsobooks.utils.TradesData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class DetailViewModel(
    private val getTickers: GetTickers,
    private val getBookTrades: GetBookTrades
) : ViewModel() {

    private val _tickerLiveData = MutableLiveData<TickerData>()
    val tickerLiveData: LiveData<TickerData>
        get() = _tickerLiveData

    private val _tradeLiveData = MutableLiveData<TradesData>()
    val tradeLiveData: LiveData<TradesData>
        get() = _tradeLiveData

    fun getTicker(book: String) = GlobalScope.launch {
        _tickerLiveData.postValue(TickerData.Loading)

        try {
            val result = getTickers(book)
            _tickerLiveData.postValue(TickerData.Success(result))
        } catch (exception: IOException) {
            _tickerLiveData.postValue(TickerData.Error(exception))
        }
    }

    fun getBookTradesList(book: String?, period: String) = GlobalScope.launch {
        _tradeLiveData.postValue(TradesData.Loading)

        try {
            val result = getBookTrades(book, period)
            _tradeLiveData.postValue(TradesData.Success(result))
        } catch (exception: IOException) {
            _tradeLiveData.postValue(TradesData.Error(exception))
        }
    }
}
