package dev.diegop88.bitsobooks.ui.detail

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import dev.diegop88.bitsobooks.R
import dev.diegop88.bitsobooks.domain.entities.Book
import dev.diegop88.bitsobooks.domain.entities.Ticker
import dev.diegop88.bitsobooks.domain.entities.Trade
import dev.diegop88.bitsobooks.utils.ChartView
import dev.diegop88.bitsobooks.utils.TickerData
import dev.diegop88.bitsobooks.utils.TradesData
import kotlinx.android.synthetic.main.activity_detail.*
import org.koin.androidx.scope.currentScope

class DetailActivity : AppCompatActivity() {

    private val viewModel: DetailViewModel by currentScope.inject()

    private var book: Book? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        book = intent.getParcelableExtra(BOOK)

        one_month.setOnClickListener { viewModel.getBookTradesList(book?.id, ONE_MONTH) }
        three_months.setOnClickListener { viewModel.getBookTradesList(book?.id, THREE_MONTHS) }
        one_year.setOnClickListener { viewModel.getBookTradesList(book?.id, ONE_YEAR) }

        book_chart.listener = ChartView.OnDataSelectedListener { date, price ->
            chart_date.text = date
            chart_price.text = price
        }

        viewModel.tickerLiveData.observe(this, Observer {
            when (it) {
                is TickerData.Loading -> showLoading()
                is TickerData.Success -> showSuccess(it.ticker)
                is TickerData.Error -> showError(it.error)
            }
        })

        viewModel.tradeLiveData.observe(this, Observer {
            when (it) {
                is TradesData.Loading -> Unit
                is TradesData.Success -> showTrades(it.tradesList)
                is TradesData.Error -> showError(it.error)
            }
        })
    }

    override fun onStart() {
        super.onStart()
        book?.let { viewModel.getTicker(it.id) }
    }

    private fun showLoading() {
        loading.visibility = View.VISIBLE
        ticker_info.visibility = View.INVISIBLE
    }

    private fun showSuccess(ticker: Ticker?) = ticker?.let {
        loading.visibility = View.GONE
        ticker_info.visibility = View.VISIBLE

        title = book?.book

        bid.apply {
            text = ticker.bid
            setTextColor(Color.parseColor(book?.color))
        }
        ask.apply {
            text = ticker.ask
            setTextColor(Color.parseColor(book?.color))
        }
        low.apply {
            text = ticker.low
            setTextColor(Color.parseColor(book?.color))
        }
        high.apply {
            text = ticker.high
            setTextColor(Color.parseColor(book?.color))
        }
        volume.apply {
            text = ticker.volume
            setTextColor(Color.parseColor(book?.color))
        }
        spread.apply {
            text = ticker.spread
            setTextColor(Color.parseColor(book?.color))
        }

        viewModel.getBookTradesList(book?.id, ONE_MONTH)
    }

    private fun showTrades(tradesList: List<Trade>) {
        book_chart.setData(tradesList)
        book_chart.setColor(book?.color)
    }

    private fun showError(error: Throwable) {
        loading.visibility = View.GONE
        Snackbar.make(loading, "Error: ${error.message}", Snackbar.LENGTH_LONG).show()
    }

    companion object {
        const val BOOK = "Book"
        const val ONE_MONTH = "1month"
        const val THREE_MONTHS = "3months"
        const val ONE_YEAR = "1year"
    }
}
