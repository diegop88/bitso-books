package dev.diegop88.bitsobooks.di

import dev.diegop88.bitsobooks.BuildConfig
import dev.diegop88.bitsobooks.data.BitsoAPI
import dev.diegop88.bitsobooks.data.BitsoMock
import dev.diegop88.bitsobooks.domain.BitsoRepository
import dev.diegop88.bitsobooks.domain.usecases.GetBookTrades
import dev.diegop88.bitsobooks.domain.usecases.GetBooks
import dev.diegop88.bitsobooks.domain.usecases.GetTickers
import dev.diegop88.bitsobooks.ui.detail.DetailActivity
import dev.diegop88.bitsobooks.ui.detail.DetailViewModel
import dev.diegop88.bitsobooks.ui.home.HomeActivity
import dev.diegop88.bitsobooks.ui.home.HomeViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private val client = OkHttpClient.Builder().apply {
    if (BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        addInterceptor(loggingInterceptor).build()
    }
}.build()

private val api = Retrofit.Builder()
    .baseUrl(BuildConfig.BASE_URL)
    .addConverterFactory(GsonConverterFactory.create())
    .client(client)
    .build().create(BitsoAPI::class.java)

val appModule = module {
    single { BitsoRepository(api) }
//    single { BitsoRepository(BitsoMock()) }

    scope(named<HomeActivity>()) {
        scoped { GetBooks(get()) }
        viewModel { HomeViewModel(get()) }
    }

    scope(named<DetailActivity>()) {
        scoped { GetTickers(get()) }
        scoped { GetBookTrades(get()) }
        viewModel { DetailViewModel(get(), get()) }
    }
}
