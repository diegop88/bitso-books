package dev.diegop88.bitsobooks.data

import dev.diegop88.bitsobooks.data.entities.BookTickerResponseEntity
import dev.diegop88.bitsobooks.data.entities.BookTradeEntity
import dev.diegop88.bitsobooks.data.entities.BooksResponseEntity
import dev.diegop88.bitsobooks.data.entities.TickerResponseEntity
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface BitsoAPI {

    @GET("available_books")
    suspend fun getBooks(): BooksResponseEntity

    @GET("ticker")
    suspend fun getTickers(): TickerResponseEntity

    @GET("ticker")
    suspend fun getTicker(@Query("book") book: String): BookTickerResponseEntity

    @GET("https://bitso.com/trade/chartJSON/{book}/{period}")
    suspend fun getBookTrade(@Path("book") book: String?, @Path("period") period: String): List<BookTradeEntity>
}
