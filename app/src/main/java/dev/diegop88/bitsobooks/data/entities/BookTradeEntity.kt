package dev.diegop88.bitsobooks.data.entities

data class BookTradeEntity(
    val date: String,
    val dated: String,
    val value: String,
    val volume: String,
    val open: String,
    val low: String,
    val high: String,
    val close: String,
    val vwap: String
)
