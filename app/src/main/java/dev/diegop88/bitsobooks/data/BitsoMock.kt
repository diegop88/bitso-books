package dev.diegop88.bitsobooks.data

import dev.diegop88.bitsobooks.data.entities.*

class BitsoMock : BitsoAPI {
    override suspend fun getBooks() = BooksResponseEntity(true, getBitcoinList())

    override suspend fun getTickers() = TickerResponseEntity(true, getTickerList())

    override suspend fun getTicker(book: String) = BookTickerResponseEntity(true, getTicker())

    override suspend fun getBookTrade(book: String?, period: String) = listOf(getBookTradeEntity())

    private fun getBitcoinList() = listOf(
        BookEntity(
            "BTC_mxn",
            "",
            "",
            "",
            "",
            "",
            ""
        )
    )

    private fun getTickerList() = listOf(getTicker())

    private fun getTicker() = TickerEntity(
        "BTC_mxn",
        "",
        "",
        "",
        "",
        "",
        "158079.09",
        "157700.00",
        ""
    )

    private fun getBookTradeEntity() = BookTradeEntity(
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "156858.69",
        ""
    )
}