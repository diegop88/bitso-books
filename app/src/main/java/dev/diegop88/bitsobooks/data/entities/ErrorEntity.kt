package dev.diegop88.bitsobooks.data.entities

data class ErrorEntity(
    val message: String,
    val code: String
)