package dev.diegop88.bitsobooks.data.entities

data class BooksResponseEntity(
    val success: Boolean,
    val payload: List<BookEntity>,
    val error: ErrorEntity? = null
)