package dev.diegop88.bitsobooks.data.entities

data class BookTickerResponseEntity(
    val success: Boolean,
    val payload: TickerEntity,
    val error: ErrorEntity? = null
)
