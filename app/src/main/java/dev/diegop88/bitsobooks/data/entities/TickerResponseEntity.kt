package dev.diegop88.bitsobooks.data.entities

data class TickerResponseEntity(
    val success: Boolean,
    val payload: List<TickerEntity>,
    val error: ErrorEntity? = null
)