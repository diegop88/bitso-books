package dev.diegop88.bitsobooks.domain.entities

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Book(
    val id: String,
    val book: String,
    val lastPrice: String?,
    val currency: String?,
    val color: String?
) : Parcelable {
    companion object {
        val diff = object : DiffUtil.ItemCallback<Book>() {
            override fun areItemsTheSame(oldItem: Book, newItem: Book) = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: Book, newItem: Book) = oldItem == newItem
        }
    }
}
