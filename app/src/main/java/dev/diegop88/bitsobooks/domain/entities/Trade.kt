package dev.diegop88.bitsobooks.domain.entities

data class Trade(val date: String, val price: String) {

    val priceValue = price.toDouble()

}
