package dev.diegop88.bitsobooks.domain.entities

data class Ticker(
    val book: String,
    val currency: String,
    val bid: String,
    val ask: String,
    val low: String,
    val high: String,
    val volume: String,
    val spread: String
)