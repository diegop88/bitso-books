package dev.diegop88.bitsobooks.domain.usecases

import dev.diegop88.bitsobooks.domain.BitsoRepository
import dev.diegop88.bitsobooks.domain.entities.Ticker
import java.text.NumberFormat
import java.util.*

class GetTickers(private val repository: BitsoRepository) {
    suspend operator fun invoke(book: String) = repository.getBookTicker(book)?.let {
        val bidPrice = it.bid.toDouble()
        val askPrice = it.ask.toDouble()
        val spread = askPrice - bidPrice
        val majorMinor = it.book.toUpperCase(Locale.getDefault()).split("_")
        Ticker(
            majorMinor[0],
            majorMinor[1],
            bindValue(majorMinor[1],it.bid),
            bindValue(majorMinor[1],it.ask),
            bindValue(majorMinor[1],it.low),
            bindValue(majorMinor[1],it.high),
            bindValue(majorMinor[1],it.volume),
            bindValue(majorMinor[1], spread.toString())
        )
    }

    private fun bindValue(currency: String, value: String): String {
        val formatPrice = if (currency == "MXN") {
            NumberFormat.getCurrencyInstance().format(value.toDoubleOrNull())
        } else {
            value
        }
        return "$formatPrice $currency"
    }
}
