package dev.diegop88.bitsobooks.domain

import dev.diegop88.bitsobooks.data.BitsoAPI
import dev.diegop88.bitsobooks.domain.entities.Trade
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class BitsoRepository(private val api: BitsoAPI) {

    suspend fun getBooks() = withContext(Dispatchers.IO) {
        val response = api.getBooks()
        if (response.success) {
            response.payload
        } else {
            emptyList()
        }
    }

    suspend fun getPrices() = withContext(Dispatchers.IO) {
        val response = api.getTickers()
        if (response.success) {
            response.payload
        } else {
            emptyList()
        }
    }

    suspend fun getBookTicker(book: String) = withContext(Dispatchers.IO) {
        val response = api.getTicker(book)
        if (response.success) {
            response.payload
        } else {
            null
        }
    }

    suspend fun getBookTrades(book: String?, period: String) = withContext(Dispatchers.IO) {
        val response = api.getBookTrade(book, period)
        if (response.isNotEmpty()) {
            response.map { Trade(it.dated, it.close) }
        } else {
            emptyList()
        }
    }
}
