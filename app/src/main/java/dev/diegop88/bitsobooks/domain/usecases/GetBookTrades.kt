package dev.diegop88.bitsobooks.domain.usecases

import dev.diegop88.bitsobooks.domain.BitsoRepository

class GetBookTrades(private val bitsoRepository: BitsoRepository) {
    suspend operator fun invoke(book: String?, period: String) = bitsoRepository.getBookTrades(book, period)
}