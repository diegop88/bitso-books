package dev.diegop88.bitsobooks.domain.usecases

import dev.diegop88.bitsobooks.domain.BitsoRepository
import dev.diegop88.bitsobooks.domain.entities.Book
import java.text.NumberFormat
import java.util.*

class GetBooks(private val repository: BitsoRepository) {
    suspend operator fun invoke(): List<Book> {
        val books = repository.getBooks()
        val tickers = repository.getPrices()
        return books.map { book ->
            val ticker = tickers.find { it.book == book.book }
            val majorMinor = book.book.toUpperCase(Locale.getDefault()).split("_")
            Book(
                book.book,
                bindBook(majorMinor[0]),
                bindLastPrice(ticker?.last, majorMinor[1]),
                majorMinor[1],
                bindColor(majorMinor[0])
            )
        }
    }

    private fun bindBook(book: String) = when (book) {
        "BTC" -> "Bitcoin"
        "ETH" -> "Ether"
        "XRP" -> "Ripple"
        "LTC" -> "Litecoin"
        "BCH" -> "BitcoinCash"
        "TUSD" -> "TrueUSD"
        "BAT" -> "Basic Attention Token"
        "GNT" -> "Golem"
        "MANA" -> "Decentraland"
        else -> "N/A"
    }

    private fun bindLastPrice(last: String?, currency: String): String {
        val formatPrice = if (currency == "MXN") {
            NumberFormat.getCurrencyInstance().format(last?.toDoubleOrNull())
        } else {
            last
        }
        return "$formatPrice $currency"
    }

    private fun bindColor(book: String) = when (book) {
        "BTC" -> "#339485"
        "ETH" -> "#489584"
        "XRP" -> "#198274"
        "LTC" -> "#918284"
        "BCH" -> "#928394"
        "TUSD" -> "#398403"
        "BAT" -> "#184923"
        "GNT" -> "#129348"
        "MANA" -> "#384958"
        else -> "#000"
    }
}
